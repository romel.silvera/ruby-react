Rails.application.routes.draw do
  get 'site/index'
  post 'api/numbers', to: 'numbers#create'
  root to: "site#index"
end
