import React, {useState } from "react";
import axios from "axios";

function app() {
  const [flag, setFlag] = useState(false);
  const [numberCollection, setCollection] = useState([]);
  const [divBy, setDiv] = useState('');

  const API_URI = "http://127.0.0.1:3000/api/numbers/";

  const handleSubmit = (e) => {
    e.preventDefault();
    const payload = { numberCollection: numberCollection, divBy: divBy };
    axios
      .post(API_URI, payload)
      .then(function (response) {
        console.log(response.data);
        setCollection(response.data.numberCollection);
        setDiv(response.data.divBy);
      })
      .catch(function (error) {
        console.log(error);
      });
    this.inputDiv.value = "";
    this.textareaCol.value = "";
    setFlag(true);
  };

  const handleDivisor = (e) => {
    let result = e.target.value.replace(/[^0-9]/g, "");
    setDiv(Number(result));
  };

  const handleCollection = (e) => {
    let arr = e.target.value.split(",");
    let nums = arr.map(function (str) {
      return parseInt(str);
    });
    setCollection(nums);
  };
  return (
    <div>
      <form className="form" onSubmit={handleSubmit}>
        <label>
          Divisor
          <input
            id="inputDiv"
            className="write-area"
            placeholder="Insert divisor value"
            onChange={handleDivisor}
            value={divBy}
            required
          ></input>
        </label>
        <label>
          Numbers collection
          <textarea
            id="textareaCol"
            className="write-area"
            rows="4"
            placeholder="Insert number values"
            onChange={handleCollection}
            required
          ></textarea>
        </label>
        <input type="submit"></input>
      </form>
      {flag && <h5>Divisor: {divBy}</h5>}
      {flag && <h4>Result collection of Numbers: {numberCollection.join(', ')}</h4>}
    </div>
  );
}

export default app;
