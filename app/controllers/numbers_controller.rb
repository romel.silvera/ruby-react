class NumbersController < ApplicationController
    # include ActionConcern
    skip_before_action :verify_authenticity_token
    attr_accessor :collection, :divisor

    def create
        @collection = params[:numberCollection]
        @divide = params[:divBy]
        collection = result_collection(@collection, @divide)
        render json: { numberCollection: collection, divBy:@divide },status: :ok
        rescue 
        render json: { numberCollection: "error" },status: :bad_request
    end

    def result_collection(number_collection,div_by)
        collection = divide_collection(number_collection,div_by)
        collection = sort_collection(collection)
        return collection
    end

    def divide_collection(number_collection, div_by)
        return number_collection.select{ |number| number % div_by != 0}
    end
    
    def sort_collection(number_collection)
        return number_collection.sort
    end

end
