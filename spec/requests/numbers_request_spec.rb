require 'rails_helper'
RSpec.describe "POST api/numbers", type: 'request' do
    it 'process a numbers collection' do
        post 'http://127.0.0.1:3000/api/numbers', params: { "numberCollection": [3,4,1,2,5,6],"divBy": 2 }
        expect(response.content_type).to start_with("application/json")
        expect(response).to have_http_status(200)
    end
end

RSpec.describe "POST api/numbers", type: 'request' do
    it 'process a numbers collection' do
        post 'http://127.0.0.1:3000/api/numbers', params: { "numberCollection": [7,2,11,3,7,8,9,2,6,7,3,8,9,14,50,30,48], "divBy": 6 }
        expect(response.content_type).to start_with("application/json")
        expect(response).to have_http_status(200)
    end
end
RSpec.describe "POST api/numbers", type: 'request' do
    it 'process a numbers collection' do
        post 'http://127.0.0.1:3000/api/numbers', params: { "numberCollection": [14,16,18,20,2,4,6,8,10,12], "divBy": 3 }
        expect(response.content_type).to start_with("application/json")
        expect(response).to have_http_status(200)
    end
end
RSpec.describe "POST api/numbers", type: 'request' do
    it 'process a numbers collection' do
        post 'http://127.0.0.1:3000/api/numbers', params: { "numberCollection": [80,70,60,50,40,30,3,2,1], "divBy": 10 }
        expect(response.content_type).to start_with("application/json")
        expect(response).to have_http_status(200)
    end
end
RSpec.describe "POST api/numbers", type: 'request' do
    it 'process a numbers collection' do
        post 'http://127.0.0.1:3000/api/numbers', params: { "numberCollection": [81,72,67,53,47,32,39,101,982,49], "divBy": 7 }
        expect(response.content_type).to start_with("application/json")
        expect(response).to have_http_status(200)
    end
end

